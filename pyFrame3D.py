#----
import numpy as np

class frame3D(object):
    """ Contains all information about a beam necessary and generated by its solution
    using classical beam theory as derived by Euler and Bernoulli. """

    def __init__(self,npos,A,Iyy,Izz,J,E,G,kDef,P):
        """Returns a frame3D object with attributes npos, A, Iyy, J, E, G, kDef, P, and npt all
        set, and all other attributes initiated with a value set to 0."""
        self.npos = npos
        self.A = A
        self.Iyy = Iyy
        self.Izz = Izz
        self.J = J
        self.E = E
        self.G = G
        self.kDef = kDef
        self.P = P
        self.nodes = self.npos.shape[0]
        self.n = self.nodes - 1
        self.dof = 6 * self.nodes
        self.L = ((((npos[1:,:] - npos[0:-1,:])**2).sum(1))**0.5)
        self.U = 0
        self.axiLoc = 0
        self.defx = 0
        self.defy = 0
        self.defz = 0
        self.Thx = 0
        self.Thy = 0
        self.Thz = 0
        self.Mv = 0
        self.Mw = 0
        self.Mtot = 0
        self.status = 'unsolved'

    def solve(self):
        """ Builds the global stiffness matrix and solves the system of equations.
        No Processing of the data is done"""

        # Rearrange kDef and P inputs
        kDef2 = self.kDef.copy().reshape(self.dof,1)
        self.P = self.P.reshape(self.dof,1)

        # Allocate the memory for the Global Stiffness Matrix
        K = np.zeros([self.dof,self.dof])

        # Loop through each beam element
        for i in range(self.n):
            # assemble the elemental stiffness matrix for each beam element - local coordinates
            c1 = self.A[i] * self.E[i] / self.L[i]
            c2 = 12 * self.E[i] * self.Izz[i] / (self.L[i])**3
            c3 = 6 * self.E[i] * self.Izz[i] / (self.L[i])**2
            c4 = 12 * self.E[i] * self.Iyy[i] / (self.L[i])**3
            c5 = 6 * self.E[i] * self.Iyy[i] / (self.L[i])**2
            c6 = self.G[i] * self.J[i] / self.L[i]
            c7 = 4 * self.E[i] * self.Iyy[i] / self.L[i]
            c8 = c7 * 0.5
            c9 = 4 * self.E[i] * self.Izz[i] / self.L[i]
            c10 = c9 * 0.5

            k = np.array([[ c1,   0,   0,   0,   0,   0, -c1,   0,   0,   0,   0,   0],
                          [  0,  c2,   0,   0,   0,  c3,   0, -c2,   0,   0,   0,  c3],
                          [  0,   0,  c4,   0, -c5,   0,   0,   0, -c4,   0, -c5,   0],
                          [  0,   0,   0,  c6,   0,   0,   0,   0,   0, -c6,   0,   0],
                          [  0,   0, -c5,   0,  c7,   0,   0,   0,  c5,   0,  c8,   0],
                          [  0,  c3,   0,   0,   0,  c9,   0, -c3,   0,   0,   0, c10],
                          [-c1,   0,   0,   0,   0,   0,  c1,   0,   0,   0,   0,   0],
                          [  0, -c2,   0,   0,   0, -c3,   0,  c2,   0,   0,   0, -c3],
                          [  0,   0, -c4,   0,  c5,   0,   0,   0,  c4,   0,  c5,   0],
                          [  0,   0,   0, -c6,   0,   0,   0,   0,   0,  c6,   0,   0],
                          [  0,   0, -c5,   0,  c8,   0,   0,   0,  c5,   0,  c7,   0],
                          [  0,  c3,   0,   0,   0, c10,   0, -c3,   0,   0,   0,  c7]])

            # Add elemental matrix to global matrix
            j1 = 6*i
            j2 = j1 + 12

            K[j1:j2, j1:j2] += k

        # Copy the Global Stiffness Matrix and Load Matrix
        Kmod = K.copy()

        # Solve system of equations for x using elimination/modification
        for i in range(self.dof):
            if np.isnan(kDef2[(i,0)]):   # check whether value is nan
                continue # if the value in kDef is not-a-number (nan), skip to next value.
            else:
                Kmod[:,i] = 0 # Replace the row with zeros
                Kmod[i,:] = 0 # Replace the column with zeros
                Kmod[i,i] = 1. # make the diagonal = 1

        self.U = np.linalg.solve(Kmod, self.P)

        # Reassemble the U vector
        for i in range(self.dof):
            if np.isnan(kDef2[(i,0)]):
                continue
            else:
                self.U[(i,0)] = kDef2[(i,0)]

        # solve for reaction forces
        self.P = K @ self.U
        self.P = self.P.reshape(self.nodes,6)

        self.status = 'solved'

    def shapeFunc(self, npt):
        # allocate memory for the shape and moment functions
        self.axiLoc = np.zeros(npt*self.n)
        self.defx = np.zeros(npt*self.n)
        self.defy = np.zeros(npt*self.n)
        self.defz = np.zeros(npt*self.n)
        self.Thx = np.zeros(npt*self.n)
        self.Thy = np.zeros(npt*self.n)
        self.Thz = np.zeros(npt*self.n)
        self.Mv = np.zeros(npt*self.n)
        self.Mw = np.zeros(npt*self.n)

        xStart = 0 # initial axial deformation

        for i in range(self.n):
            # create a vector of locations along the element x-axis
            xloc = np.linspace(0,self.L[i],npt)

            # generate cubic functions - position
            n1 = (1/self.L[i]**3) * (self.L[i]**3 - 3*self.L[i]*(xloc**2) + 2*(xloc**3))
            n2 = (1/self.L[i]**2) * ((self.L[i]**2)*xloc - 2*self.L[i]*(xloc**2) + (xloc**3))
            n3 = (1/self.L[i]**3) * (3*self.L[i]*(xloc**2) - 2*(xloc**3))
            n4 = (1/self.L[i]**2) * (-self.L[i]*(xloc**2) + (xloc**3))
            # first derivative - angle
            ndot1 = (1/self.L[i]**3) * (-6*self.L[i]*xloc + 6*(xloc**2))
            ndot2 = (1/self.L[i]**2) * (self.L[i]**2 - 4*self.L[i]*xloc + 3*(xloc**2))
            ndot3 = (1/self.L[i]**3) * (6*self.L[i]*xloc - 6*(xloc**2))
            ndot4 = (1/self.L[i]**2) * (3*(xloc**2) - 2*self.L[i]*xloc)
            # second derivative - bending moment (/ EI)
            n2dot1 = (1/self.L[i]**3) * (12*(xloc) - 6*self.L[i])
            n2dot2 = (1/self.L[i]**2) * (6.*xloc - 4*self.L[i])
            n2dot3 = (1/self.L[i]**3) * (6*self.L[i] - 12*xloc)
            n2dot4 = (1/self.L[i]**2) * (6*xloc - 2*self.L[i])

            # retrieve the element U vector, Un
            j1 = 6*i
            j2 = j1 + 12
            un = self.U[j1:j2,0]

            # ---- SHAPE FUNCTIONS ----
            # deflection vectors
            ux = (un[0] / self.L[i]) * xloc
            vx = n1*un[1] + n2*un[5] + n3*un[7] + n4*un[11]
            wx = n1*un[2] - n2*un[4] + n3*un[8] - n4*un[10]
            # angular deflection vectors
            txx = (un[3] / self.L[i]) * xloc
            tyx = ndot1*un[2] - ndot2*un[4] + ndot3*un[8] - ndot4*un[10]
            tzx = ndot1*un[1] + ndot2*un[5] + ndot3*un[7] + ndot4*un[11]
            # moment vectors
            mv = self.E[i] * self.Izz[i] *(n2dot1*un[1] + n2dot2*un[5] + n2dot3*un[7] + n2dot4*un[11])
            mw = self.E[i] * self.Izz[i] *(n2dot1*un[2] - n2dot2*un[4] + n2dot3*un[8] - n2dot4*un[10])

            # update axial location information
            xlocmod = (xloc + ux)
            modXloc = xlocmod + xStart
            xStart += xlocmod[-1]

            # add content to the global vectors
            idx1 = npt*i
            idx2 = idx1 + npt

            self.axiLoc[idx1:idx2] = modXloc
            self.defx[idx1:idx2] = ux
            self.defy[idx1:idx2] = vx
            self.defz[idx1:idx2] = wx
            self.Thx[idx1:idx2] = txx
            self.Thy[idx1:idx2] = tyx
            self.Thz[idx1:idx2] = tzx
            self.Mv[idx1:idx2] = mv
            self.Mw[idx1:idx2] = mw
            self.Mtot = ((mv**2) + (mw**2))**0.5

        # change status of frame b
        self.status = 'processed'

    def maxDef(self):
        """ Returns the maximum deflection in the beam """
        Defl =((self.defx)**2 + (self.defy)**2 + (self.defz)**2)**0.5
        maxDefl = np.round(np.max(Defl),4)
        arg = np.argmax(Defl)
        axiLocMax = self.axiLoc[arg]
        return (maxDefl, axiLocMax)

    def maxBendMom(self):
        """ Returns the largest magnitude of bending moment in the beam """
        maxMom = np.round(np.max(self.Mtot),4)
        arg= np.argmax(self.Mtot)
        axiLocMax = self.axiLoc[arg]
        return (maxMom, axiLocMax)

    def constrNodes(self):
        """ Returns the index of every constrained node"""
        nconstr = []
        for i in range(self.nodes):
            n_nan = 0
            for j in range(6):
                if np.isnan(self.kDef[i,j]):
                    n_nan += 1
                else:
                    continue
            if n_nan == 6:
                continue
            else:
                nconstr.append(i)

        return nconstr

    def nodeload(self, node):
        """ Returns the loads for a particular node """
        if node <= self.nodes:
            i = 6*node
            loads = self.P[i:i+6]
            print('Fx, Fy, Fz, Mx, My, Mz')
            print(loads)
        else:
            print('ERROR: That node does not exist.')

    def changeLoad(self, Pnew):
        """ Changes the load vector """
        if Pnew.shape == self.P.shape:
            self.P = Pnew
            self.nodes = self.npos.shape[0]
            self.n = self.nodes - 1
            self.dof = 6 * self.nodes
            self.L = ((((npos[1:,:] - npos[0:-1,:])**2).sum(1))**0.5)
            self.U = 0
            self.axiLoc = 0
            self.defx = 0
            self.defy = 0
            self.defz = 0
            self.Thx = 0
            self.Thy = 0
            self.Thz = 0
            self.Mv = 0
            self.Mw = 0
            self.Mtot = 0
            self.status = 'unsolved'
        else:
             print('ERROR: New loading input is the wrong shape')

    def nodeConstrain(self, node):
        """ Contrains the input node with deflections set to zero """
        if node <= self.nodes:
            kDef[node,:] = 0.
            self.nodes = self.npos.shape[0]
            self.n = self.nodes - 1
            self.dof = 6 * self.nodes
            self.L = ((((npos[1:,:] - npos[0:-1,:])**2).sum(1))**0.5)
            self.U = 0
            self.axiLoc = 0
            self.defx = 0
            self.defy = 0
            self.defz = 0
            self.Thx = 0
            self.Thy = 0
            self.Thz = 0
            self.Mv = 0
            self.Mw = 0
            self.Mtot = 0
            self.status = 'unsolved'
        else:
            print('ERROR: That node does not exist.')

    def nodeConstrainDef(self, node, dx, dy, dz, thx, thy, thz):
        """ Constrains the input node with user set deflections """
        if node <= self.nodes:
            kDef[node,:] = [dx, dy, dz, thx, thy, thz]
            self.nodes = self.npos.shape[0]
            self.n = self.nodes - 1
            self.dof = 6 * self.nodes
            self.L = ((((npos[1:,:] - npos[0:-1,:])**2).sum(1))**0.5)
            self.U = 0
            self.axiLoc = 0
            self.defx = 0
            self.defy = 0
            self.defz = 0
            self.Thx = 0
            self.Thy = 0
            self.Thz = 0
            self.Mv = 0
            self.Mw = 0
            self.Mtot = 0
            self.status = 'unsolved'
        else:
            print('ERROR: That node does not exist.')

    def checkSingularity(self):
        """ Checks whether the problem is fully constrained """
         # loop through each column
        DOF = ['delta x','delta y','delta z','theta x','theta y','theta z']
        unconstrained = []

        for j in range(6):
            # check whether there is a non nan value in the column
            n_nan = 0
            for i in range(self.nodes):
                if np.isnan(self.kDef[i,j]):
                    continue
                else:
                    n_nan += 1

            if n_nan > 0:
                continue
            else:
                unconstrained.append(DOF[j])

        if unconstrained == []:
            print('System is fully constrained')
        else:
            print('System is not constrained in the following degrees of freedom:')
            print(unconstrained)

    def addPointLoad(self,W,loc):
        """ Adds a point load, W, entered as a tuple of length 3 with each entry
        corresponding the X, Y, and Z components of the force vector """
        if loc in self.npos[:, 0]:
            idx = np.argmin(abs(self.npos[:,0] - loc))
            self.P[idx,0] = W[0]
            self.P[idx,1] = W[1]
            self.P[idx,2] = W[2]
        else:
            idx = np.searchsorted(self.npos[:,0], loc) - 1
            b = loc - self.npos[idx,0]
            a = self.npos[idx+1,0] - loc
            le = self.L[idx]
            self.P[idx,0] += 0.5*W[0]
            self.P[idx+1,0] += 0.5*W[0]
            self.P[idx,1] += W[1]*(b**2)*(le + 2*a)/(le**3)
            self.P[idx+1,1] += W[1]*(a**2)*(le + 2*b)/(le**3)
            self.P[idx,2] += W[2]*(b**2)*(le + 2*a)/(le**3)
            self.P[idx+1,2] += W[2]*(a**2)*(le + 2*b)/(le**3)
            self.P[idx,4] += (W[2]*a*b**2)/(le**2)
            self.P[idx+1,4] -= (W[2]*b*a**2)/(le**2)
            self.P[idx,5] += (W[1]*a*b**2)/(le**2)
            self.P[idx+1,5] -= (W[1]*b*a**2)/(le**2)

    def addDstrLoad(self,w,element):
        """ Adds a constant distributed load to the structure """
        force = 0.5*self.L[element]*w
        #print('forces = ',force)
        moment = (1/12)*(self.L[element]**2)*w
        #print('moment = ',moment)
        self.P[element,0] += force[0]
        self.P[element+1,0] += force[0]
        self.P[element,1] += force[1]
        self.P[element+1,1] += force[1]
        self.P[element,2] += force[2]
        self.P[element+1,2] += force[2]
        self.P[element,4] += moment[2]
        self.P[element+1,4] -= moment[2]
        self.P[element,5] += moment[1]
        self.P[element+1,5] -= moment[1]
