# pyFrameSolver_3D

A finite-element frame-element solver for a linear frame element with 6 degrees of freedom at each node.  Designed to be easily scriptable for integration with other PDE solvers and for use in optimization.

